package com.example.ty.currencyconverter;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Lab1 extends AppCompatActivity {
    //Declare some variables
    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String usd;

    String url = "https://api.fixer.io/latest?base=USD";
    String json = "";

    String line = "";
    //target
    String rate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab1);
        //cast the variables to their ids
        editText01 = findViewById(R.id.EditText01);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);


        //Click Event
        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {
                BackgroundTask object = new BackgroundTask();
                object.execute();
            }
        });
    }

    private class BackgroundTask extends AsyncTask<Void, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
        }

        @Override
        protected String doInBackground(Void... params){

            try {
                URL web_url = new URL(Lab1.this.url);

                HttpURLConnection httpURLConnection = (HttpURLConnection)web_url.openConnection();

                httpURLConnection.setRequestMethod("GET");

                httpURLConnection.connect();


                InputStream inputStream = httpURLConnection.getInputStream();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                System.out.println("CONNECTION SUCCESSFUL");

                while(line != null){
                    line = bufferedReader.readLine();
                    json += line;
                }

                System.out.println("\nTHE JSON: " + json);

                JSONObject obj = new JSONObject(json);

                JSONObject objRate = obj.getJSONObject("rates");

                rate = objRate.get("JPY").toString();

                System.out.println("What is the Rate?" + rate);

                Double value = Double.parseDouble(rate);

                //convert user's input tp string
                usd = editText01.getText().toString();
                //if-else statement to not leave the field blank
                if(usd.equals("")){
                    textView01.setText("This field cannot be blank!");
                } else {
                    Double dInputs = Double.parseDouble(usd);
                    Double result = dInputs * value;
                    textView01.setText("$" + usd + " = " + "¥" + String.format("%.2f", result));
                    editText01.setText("");
                }



            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception");
            }

            return null;
        }
    }
}
