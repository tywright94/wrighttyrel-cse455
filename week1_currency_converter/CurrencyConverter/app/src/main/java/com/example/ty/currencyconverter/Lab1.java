package com.example.ty.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Lab1 extends AppCompatActivity {
    //Declare some variables
    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String usd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab1);
        //cast the variables to their ids
        editText01 = findViewById(R.id.EditText01);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);


        //Click Event
        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {
                //convert user's input tp string
                usd = editText01.getText().toString();
                //if-else statement to not leave the field blank
                if(usd.equals("")){
                    textView01.setText("This field cannot be blank!");
                } else {
                    Double dInputs = Double.parseDouble(usd);
                    Double result = dInputs * 112.57;
                    textView01.setText("$" + usd + " = " + "¥"+String.format("%.2f",result));
                    editText01.setText("");
                }
            }
        });
    }
}
